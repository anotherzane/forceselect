(function () {
  "use strict";

  const style = document.createElement("style");
  style.innerHTML =
    "*{ user-select: auto !important; -webkit-user-select: auto !important; }";

  (document.head || document.documentElement).appendChild(style);
})();
